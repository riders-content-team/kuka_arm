#!/usr/bin/env python3
import rospy
import sys
from std_msgs.msg import Float64
import math
import rospy
import tf
#from kuka_arm.srv import *
from trajectory_msgs.msg import JointTrajectory, JointTrajectoryPoint
from geometry_msgs.msg import Pose
from sympy import *
import sympy as sym
from sympy.matrices import Matrix
from mpmath import *
import time

def talker():
    #pub1 = rospy.Publisher('/kuka_arm/joint1_position_controller/command', Float64, queue_size=10)
    #pub2 = rospy.Publisher('/kuka_arm/joint2_position_controller/command', Float64, queue_size=10)
    #pub3 = rospy.Publisher('/kuka_arm/joint3_position_controller/command', Float64, queue_size=10)
    #pub4 = rospy.Publisher('/kuka_arm/joint4_position_controller/command', Float64, queue_size=10)
    #pub5 = rospy.Publisher('/kuka_arm/joint5_position_controller/command', Float64, queue_size=10)
    #pub6 = rospy.Publisher('/kuka_arm/joint6_position_controller/command', Float64, queue_size=10)

    joints = ["joint_1", "joint_2", "joint_3", "joint_4", "joint_5", "joint_6"]
    #joints = ["kuka_arm::joint_1", "kuka_arm::joint_2", "kuka_arm::joint_3", "kuka_arm::joint_4"," kuka_arm::joint_5"," kuka_arm::joint_6"]

    rospy.init_node('talker', anonymous=True)

    print(rospy.get_rostime().to_sec())
    while rospy.get_rostime().to_sec() == 0.0:
        time.sleep(0.1)
        print(rospy.get_rostime().to_sec())

    pub = rospy.Publisher('/kuka_arm/gazebo_ros_control/command', JointTrajectory, queue_size=10)
    jt = JointTrajectory()
    dt = 0.001

    jt.header.stamp = rospy.Time.now()
    jt.header.frame_id = ""

    for joint in joints:
        jt.joint_names.append(joint)

    rate = rospy.Rate(10000)

    px = -1.0
    py = 1.0
    pz = 0.0
    while not rospy.is_shutdown():

        req = []
        # D-H parameters syms - length, offset and rotational angles
        d1, d2, d3, d4, d5, d6, d7 = sym.symbols('d1:8')
        a0, a1, a2, a3, a4, a5, a6 = sym.symbols('a0:7')
        q1, q2, q3, q4, q5, q6, q7 = sym.symbols('q1:8')
        alpha0, alpha1, alpha2, alpha3, alpha4, alpha5, alpha6 = sym.symbols('alpha0:7')

        # Distance and Angle (in meters and radians)
        d1 , d3_4 , d7, a1_2, a2_3, a3_4 = 0.75 , 1.5, 0.303, 0.35, 1.25, -0.054
        alpha0 = alpha2 = alpha6 = a0 = a4 = a5 = a6 = d2 = d3 = d5 = d6 = q1 = q7 = 0
        alpha1 = alpha3 = alpha5 = aplha1_2 = alpha3_4 = -math.pi/2
        alpha4 = aplha4_5 = math.pi/2


        # D-H parameters specifics for KUKA KR210 from FWD Kinematics Section
        DH_Table = {    alpha0:      0, 	a0:      0,   d1:  0.75,   q1:         q1,
                        alpha1:  (-math.pi)/2, 	a1:   0.35,   d2:     0,   q2: (-math.pi)/2 + q2,
                        alpha2:      0, 	a2:   1.25,   d3:     0,   q3:         q3,
                        alpha3:  (-math.pi)/2, 	a3: -0.054,   d4:   1.5,   q4:         q4,
                        alpha4:   (math.pi)/2, 	a4:      0,   d5:     0,   q5:         q5,
                        alpha5:  (-math.pi)/2, 	a5:      0,   d6:     0,   q6:         q6,
                        alpha6:      0, 	a6:      0,   d7: 0.303,   q7:          0,}



        # Define Modified DH Transformation matrix
        def TF_MAT (alpha, a, d, q):
            TF = Matrix([[          math.cos(0),           -math.sin(0),           0,             a],
                         [ math.sin(0)*math.cos(alpha), math.cos(0)*math.cos(alpha), -math.sin(alpha), -math.sin(alpha)*d],
                         [ math.sin(0)*math.sin(alpha), math.cos(0)*math.sin(alpha),  math.cos(alpha),  math.cos(alpha)*d],
                         [                 0,                 0,           0,             1]])
            return TF


        # Extract end-effector position and orientation from request
        # px,py,pz = end-effector position
        # roll, pitch, yaw = end-effector orientation
        r = 30
        p = 30
        y = 30

        # Calculate joint angles using Geometric IK method
        # R_rpy = roll, pitch, yaw
        R_rpy =  Matrix([[math.cos(y)*math.cos(p), math.cos(y)*math.sin(p)*math.sin(r)-math.sin(y)*math.cos(r), math.cos(y)*math.sin(p)*math.cos(r)+math.sin(y)*math.sin(r)],
                         [math.sin(y)*math.cos(p), math.sin(y)*math.sin(p)*math.sin(r)+math.cos(y)*math.cos(r), math.sin(y)*math.sin(p)*math.cos(r)-math.cos(y)*math.sin(r)],
                         [-math.sin(p),         math.cos(p)*math.sin(r),                             math.cos(p)*math.cos(r)]])

        # Compute correlation matrix
        R_correlation = Matrix([[0,0,1],
                                [0,-1,0],
                                [1,0,0]])
        # Calculate the origin matrix by the rpy and correlation transpose
        R_rpy0_6 = R_rpy*(R_correlation.T)

        # Calculate the end-effector matrix
        EE = Matrix([[px],
                     [py],
                     [pz]])

        # Calculate wrist center
        wc = EE - d7 * R_rpy0_6[:,2]
        print("The wrist center is:",wc)

        # Acquire angle theta1
        theta1 = math.atan2(wc[1],wc[0])

        # Use SSS triangle and Law of Sines with line segment lengths
        seg1_3 = 1.25
        seg3_6 = 1.501
        seg1_6 = math.sqrt(pow((math.sqrt(wc[0] * wc[0]+wc[1] * wc[1]) - 0.35),2)+pow((wc[2] - 0.75), 2))

        # Take the inverse cosine to get the angle (phi)
        phi1 = acos((seg1_6 * seg1_6 + seg1_3*seg1_3 -seg3_6 * seg3_6) / (2 * seg1_6 * seg1_3))
        phi2 = acos((seg3_6 * seg3_6 + seg1_3*seg1_3 -seg1_6 * seg1_6) / (2 * seg3_6 * seg1_3))

        # Calculate theta2 and theta3
        theta2 = math.pi/2 - phi1 - math.atan2((wc[2]-0.75),(math.sqrt(wc[0] * wc[0]+wc[1] * wc[1]) - 0.35))
        theta3 = math.pi/2 - phi2 - 0.03598 #fixed angle = atan2(0.054,1.5)

        # Create individual transformation matrices
        T0_1 = TF_MAT(alpha0,a0,d1,q1).subs(DH_Table)
        T1_2 = TF_MAT(alpha1,a1,d2,q2).subs(DH_Table)
        T2_3 = TF_MAT(alpha2,a2,d3,q3).subs(DH_Table)
        T3_4 = TF_MAT(alpha3,a3,d4,q4).subs(DH_Table)
        T4_5 = TF_MAT(alpha4,a4,d5,q5).subs(DH_Table)
        T5_6 = TF_MAT(alpha5,a5,d6,q6).subs(DH_Table)
        T6_7 = TF_MAT(alpha6,a6,d7,q7).subs(DH_Table)

        # Final product of matrix multiplication
        T0_7 = T0_1 * T1_2 * T2_3 * T3_4 * T4_5 * T5_6 * T6_7

        # Mathematical equivalents
        T0_2 = T0_1 * T1_2
        T0_3 = T0_2 * T2_3
        T0_4 = T0_3 * T3_4
        T0_5 = T0_4 * T4_5
        T0_6 = T0_5 * T5_6

        # Project transformation matrices to rotational matrices
        R0_1 = T0_3[0:3,0:3]
        R0_2 = T0_2[0:3,0:3]
        R0_3 = T0_3[0:3,0:3]
        R0_4 = T0_4[0:3,0:3]
        R0_5 = T0_5[0:3,0:3]
        R0_6 = T0_6[0:3,0:3]
        R0_7 = T0_7[0:3,0:3]


        # Calculate rotational matrices
        R_rpy0_3 = R0_3.evalf(subs={q1: theta1, q2: theta2, q3: theta3})
        R_rpy3_6 = R_rpy0_3.T * R_rpy0_6

        # Use the previous information to acquire the remaining angles (theta)
        theta4 = math.atan2(R_rpy3_6[2,2], -R_rpy3_6[0,2])
        theta5 = math.atan2(sqrt(R_rpy3_6[0,2]*R_rpy3_6[0,2] + R_rpy3_6[2,2]*R_rpy3_6[2,2]),R_rpy3_6[1,2])
        theta6 = math.atan2(-R_rpy3_6[1,1],R_rpy3_6[1,0])


        # Populate response for the IK request
        # In the next line replace theta1,theta2...,theta6 by your joint angle variables
        #joint_trajectory_point.positions = [theta1, theta2, theta3, theta4, theta5, theta6]
        #joint_trajectory_list.append(joint_trajectory_point)

        #rospy.loginfo("length of Joint Trajectory List: %s" % len(joint_trajectory_list))
        rospy.loginfo(theta1)
        rospy.loginfo(theta2)
        rospy.loginfo(theta3)
        rospy.loginfo(theta4)
        rospy.loginfo(theta5)
        rospy.loginfo(theta6)
        #pub1.publish(theta1)
        #pub2.publish(theta2)
        #pub3.publish(theta3)
        #pub4.publish(theta4)
        #pub5.publish(theta5)
        #pub6.publish(theta6)

        thetas = [theta1, theta2, theta3, theta4, theta5, theta6]
        p = JointTrajectoryPoint()

        for i, theta in enumerate(thetas):
            p.positions.append(theta)
            p.time_from_start = rospy.Duration.from_sec(dt + rospy.get_rostime().to_sec())

        jt.points.append(p)

        pub.publish(jt)
        jt.points = []

        #break
        rate.sleep()

if __name__ == '__main__':
    try:
        talker()
    except rospy.ROSInterruptException:
        pass
